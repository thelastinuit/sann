defmodule SANN.Layer do
  @moduledoc """
  It handles a group of neurons
  """

  alias SANN.{Layer, Neuron}

  defstruct process_id: nil, neurons: []

  @doc """
  Init layer
  """
  def start_link(attributes \\ %{}) do
    {:ok, process_id} = Agent.start_link(fn -> %Layer{} end)
    neurons = process_neurons(Map.get(attributes, :neuron_size))
    process_id
    |> update(%{process_id: process_id, neurons: neurons})
    {:ok, process_id}
  catch
    :error, reason -> {:error, reason}
  end

  @doc """
  Finds layer
  """
  def find(process_id), do: Agent.get(process_id, & &1)

  @doc """
  """
  def update(process_id, attributes) do
    Agent.update(process_id, &Map.merge(&1, attributes))
    {:ok, process_id}
  catch
    :error, reason -> {:error, reason}
  end

  @doc """
  Adds neurons to layer
  """
  def add(process_id, neurons) do
    process_id
    |> update(%{neurons: find(process_id).neurons ++ neurons})
    {:ok, process_id}
  catch
    :error, reason -> {:error, reason}
  end

  @doc """
  Clear layer.
  """
  def clear(process_id) do
    process_id
    |> update(%{neurons: []})
  end

  @doc """
  Init layer with neurons
  """
  def set(process_id, neurons) do
    process_id
    |> clear

    process_id
    |> add(neurons)
  end

  @doc """
  Trains neurons in layers
  """
  def train(layer, target_outputs \\ []) do
    layer.neurons
    |> Stream.with_index()
    |> Enum.each(fn tuple ->
      {neuron, index} = tuple
      neuron
      |> Neuron.train(Enum.at(target_outputs, index))
    end)
    {:ok, "Successfully trained neurons in layer"}
  catch
    :error, reason -> {:error, reason}
  end

  @doc """
  Entangle neurons between layers
  """
  def entangle(input_process_id, output_process_id) do
    input_layer = find(input_process_id)

    unless contains_bias?(input_layer) do
      {:ok, process_id} = Neuron.start_link(%{bias?: true})
      input_process_id
      |> add([process_id])
    end

    for source_neuron <- find(input_process_id).neurons,
      target_neuron <- find(output_process_id).neurons do
        Neuron.entangle(source_neuron, target_neuron)
    end

    {:ok, "Neuron entanglement successfully"}
  catch
    :error, reason -> {:error, reason}
  end

  @doc """
  Activates layer neurons
  """
  def activate(process_id, values \\ nil) do
    layer = find(process_id)
    values = List.wrap(values)

    layer.neurons
    |> Stream.with_index()
    |> Enum.each(fn tuple ->
      {neuron, index} = tuple
      neuron
      |> Neuron.activate(Enum.at(values, index))
    end)

    {:ok, "Layer activation successfully"}
  catch
    :error, reason -> {:error, reason}
  end

  defp contains_bias?(layer) do
    Enum.any?(layer.neurons, &Neuron.find(&1).bias?)
  end

  defp process_neurons(nil), do: []
  defp process_neurons(size) when size < 1, do: []
  defp process_neurons(size) when size > 0 do
    Enum.into(1..size, [], fn _ ->
      {:ok, process_id} = Neuron.start_link()
      process_id
    end)
  end
end
