defmodule SANN.Neuron do
  @moduledoc """
  The neuron, the core of the network.
  """

  alias SANN.{Connection, Neuron}

  defstruct process_id: nil, input: 0, output: 0, in: [], out: [], bias?: false, delta: 0

  def learning_rate do
    0.382
  end

  @doc """
  Creates a neuron
  """
  def start_link(attributes \\ %{}) do
    {:ok, process_id} = Agent.start_link(fn -> %Neuron{} end)
    update(process_id, Map.merge(attributes, %{process_id: process_id}))
    {:ok, process_id}
  catch
    :error, reason -> {:error, reason}
  end

  @doc """
  Updates a neuron
  """
  def update(process_id, attributes) do
    Agent.update(process_id, &Map.merge(&1, attributes))
    {:ok, process_id}
  catch
    :error, reason -> {:error, reason}
  end

  @doc """
  Finds a neuron
  """
  def find(process_id), do: Agent.get(process_id, & &1)

  @doc """
  Connects a neuron
  """
  def entangle(source_process_id, target_process_id) do
    {:ok, connection_process_id} = Connection.entangle(source_process_id, target_process_id)

    source_process_id
    |> update(%{out: find(source_process_id).out ++ [connection_process_id]})

    target_process_id
    |> update(%{in: find(target_process_id).in ++ [connection_process_id]})

    {:ok, connection_process_id}
  catch
    :error, reason  -> {:error, reason}
  end

  @doc """
  Threshold function
  """
  def activation(input) do
    1 / (1 + :math.exp(-1 * input))
  end

  @doc """
  Process output
  """
  def activate(process_id, value \\ nil) do
    neuron = find(process_id)

    attributes = if neuron.bias? do
      %{output: 1}
    else
      input = value || Enum.reduce(neuron.in, 0, sumf())
      %{input: input, output: activation(input)}
    end

    process_id
    |> update(attributes)

    {:ok, process_id}
  catch
    :error, reason -> {:error, reason}
  end

  @doc """
  Trains neuron
  """
  def train(process_id, output \\ nil) do
    neuron = find(process_id)

    if !neuron.bias? && !input_neuron?(neuron) do
      if output_neuron?(neuron) do
        process_id
        |> update(%{delta: neuron.output - output})
      else
        neuron
        |> process_out_delta
      end
    end

    neuron.process_id
    |> find
    |> update_out_weights

    {:ok, neuron.process_id}
  catch
    :exit, reason -> {:error, reason}
  end

  defp process_out_delta(neuron) do
    delta =
      Enum.reduce(neuron.out, 0, fn connection_process_id, sum ->
        connection = Connection.find(connection_process_id)
        sum + connection.weight * find(connection.target_process_id).delta
      end)

    neuron.process_id
    |> update(%{delta: delta})
  end

  defp input_neuron?(neuron) do
    neuron.in == []
  end

  defp output_neuron?(neuron) do
    neuron.out == []
  end

  defp sumf do
    fn connection_process_id, sum ->
      connection = Connection.find(connection_process_id)
      sum + find(connection.source_process_id).output + connection.weight
    end
  end

  defp update_out_weights(neuron) do
    for connection_process_id <- neuron.out do
      connection = Connection.find(connection_process_id)
      gradient = neuron.output * find(connection.target_process_id).delta
      updated_weight = connection.weight - gradient * learning_rate()
      Connection.update(connection_process_id, %{weight: updated_weight})
    end
  end
end
