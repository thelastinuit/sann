defmodule SANN.Network do
  @moduledoc """
  It handles groups of layers
  """
  alias SANN.{Layer, Network, Neuron}

  defstruct process_id: nil, input_layer: nil, hidden_layers: [], output_layer: nil, error: 0

  @doc """
  Init network
  """
  def start_link(layer_sizes \\ []) do
    {:ok, process_id} = Agent.start_link(fn -> %Network{} end)

    layers =
      map_layers(
        input_neurons(layer_sizes),
        hidden_neurons(layer_sizes),
        output_neurons(layer_sizes)
      )

    process_id
    |> update(layers)

    process_id
    |> entangle_layers

    {:ok, process_id}
  end

  @doc """
  Finds network
  """
  def find(process_id), do: Agent.get(process_id, & &1)

  @doc """
  Updates network
  """
  def update(process_id, attributes) do
    attributes = Map.merge(attributes, %{process_id: process_id})
    Agent.update(process_id, &Map.merge(&1, attributes))
    {:ok, process_id}
  catch
    :error, reason -> {:error, reason}
  end

  @doc """
  Trains network
  """
  def train(network, target_outputs) do
    network.output_layer
    |> Layer.find
    |> Layer.train(target_outputs)

    network.process_id
    |> update(%{error: error_function(network, target_outputs)})

    network.hidden_layers
    |> Enum.reverse()
    |> Enum.each(fn layer_process_id ->
      layer_process_id
      |> Layer.find
      |> Layer.train(target_outputs)
    end)

    network.input_layer
    |> Layer.find
    |> Layer.train(target_outputs)
  end

  @doc """
  Activates network
  """
  def activate(network, values) do
    network.input_layer
    |> Layer.activate(values)

    Enum.each(network.hidden_layers, fn hidden_layer ->
      hidden_layer
      |> Layer.activate()
    end)

    network.output_layer
    |> Layer.activate()
  end

  defp input_neurons(layer_sizes) do
    size = layer_sizes
    |> List.first()
    {:ok, process_id} = Layer.start_link(%{neurons_size: size})
    process_id
  end

  defp hidden_neurons(layer_sizes) do
    layer_sizes
    |> hidden_layer_slice
    |> Enum.map(fn size ->
      {:ok, process_id} = Layer.start_link(%{neuron_size: size})
      process_id
    end)
  end

  defp output_neurons(layer_sizes) do
    size = layer_sizes
    |> List.last()
    {:ok, process_id} = Layer.start_link(%{neuron_size: size})
    process_id
  end

  defp hidden_layer_slice(layer_sizes) do
    layer_sizes
    |> Enum.slice(1..(length(layer_sizes) - 2))
  end

  defp entangle_layers(process_id) do
    layers = process_id
    |> Network.find()
    |> flatten_layers

    layers
    |> Stream.with_index()
    |> Enum.each(fn tuple ->
      {layer, index} = tuple
      next_index = index + 1

      if Enum.at(layers, next_index) do
        Layer.entangle(layer, Enum.at(layers, next_index))
      end
    end)
  end

  defp flatten_layers(network) do
    [network.input_layer] ++ network.hidden_layers ++ [network.output_layer]
  end

  defp error_function(network, target_outputs) do
    (Layer.find(network.output_layer).neurons
    |> Stream.with_index()
    |> Enum.reduce(0, fn {neuron_process_id, index}, sum ->
        target_output = Enum.at(target_outputs, index)
        actual_output = Neuron.find(neuron_process_id).output
        squared_error(sum, target_output, actual_output)
      end)) / length(Layer.find(network.output_layer).neurons)
  end

  defp squared_error(sum, target_output, actual_output) do
    sum + 0.5 * :math.pow(target_output - actual_output, 2)
  end

  defp map_layers(input_layer, hidden_layers, output_layer) do
    %{
      input_layer: input_layer,
      hidden_layers: hidden_layers,
      output_layer: output_layer
    }
  end
end
