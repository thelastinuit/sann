defmodule SANN.Connection do
  @moduledoc """
  This is the module in charge of neuron connections.
  """
  alias SANN.{Connection}

  defstruct process_id: nil, source_process_id: nil, target_process_id: nil, weight: :rand.uniform()

  @doc """
  Init connection.
  """
  def start_link(attributes \\ %{}) do
    {:ok, process_id} = Agent.start_link(fn -> %Connection{} end)
    update(process_id, Map.merge(attributes, %{process_id: process_id}))
    {:ok, process_id}
  catch
    :error, reason -> {:error, reason}
  end

  @doc """
  Finds connection.
  """
  def find(process_id), do: Agent.get(process_id, & &1)

  @doc """
  Updates connection.
  """
  def update(process_id, attributes) do
    Agent.update(process_id, fn connection -> Map.merge(connection, attributes) end)
    Agent.update(process_id, &Map.merge(&1, attributes))
    {:ok, process_id}
  catch
    :error, reason -> {:error, reason}
  end

  @doc """
  Connects two neurons.
  """
  def entangle(source_process_id, target_process_id) do
    {:ok, process_id} = start_link()
    process_id |> update(%{source_process_id: source_process_id, target_process_id: target_process_id})
    {:ok, process_id}
  catch
    :error, reason -> {:error, reason}
  end
end
