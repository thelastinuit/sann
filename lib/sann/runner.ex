defmodule SANN.Runner do
  @moduledoc """
  Runs the network.
  """

  alias SANN.{Network}

  def process(network_process_id, data, options \\ %{}) do
    epochs = options.epochs
    log_frequencies = options.log_frequencies
    data_length = length(data)

    for epoch <- 0..epochs do
      average_error =
        Enum.reduce(data, 0, fn sample, sum ->
          network_process_id
          |> Network.find()
          |> Network.activate(sample.input)

          network_process_id
          |> Network.find()
          |> Network.train(sample.output)

          sum + Network.find(network_process_id).error / data_length
        end)

      if rem(epoch, log_frequencies) == 0 || epoch + 1 == epochs do
        IO.puts("Epoch: #{epoch} Error: #{unexponential(average_error)}")
      end
    end

    {:ok, "Successfully executed"}
  catch
  :error, reason -> {:error, reason}
  end

  defp unexponential(average_error) do
    :erlang.float_to_binary(average_error, [{:decimals, 19}, :compact])
  end
end
