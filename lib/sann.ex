defmodule SANN do
  @moduledoc """
  SANN: Simple Artificial Neural Network
  """

  @doc """
  Start
  ## Examples

      iex> SANN.start()
      :world
  """

  use Application

  def start(_type, _args) do
    import Supervisor.Spec, warn: false

    children = [
      # worker(SANN.Worker, [args**])
    ]

    otps = [strategy: :one_for_one, name: SAN.Supervisor]
    Supervisor.start_link(children, otps)
  end
end
