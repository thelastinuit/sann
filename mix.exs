defmodule SANN.MixProject do
  use Mix.Project

  def project do
    [
      app: :sann,
      version: "0.1.0",
      name: "SANN: Simple Artificial Neural Network",
      elixir: "~> 1.7",
      description: description(),
      package: package(),
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      preferred_cli_env: [coveralls: :test, "coveralls.detail": :test, "coveralls.post": :test, "coveralls.html": :test, espec: :test],
      test_coverage: [tool: ExCoveralls, test_task: "espec"]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {SANN, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:excoveralls, "~> 0.10", only: :test},
      {:espec, "~> 1.6.3", only: :test},
      {:credo, "~> 1.0.0", only: [:dev, :test], runtime: false}
    ]
  end

  defp description do
    """
    A simple neural network made.
    """
  end

  defp package do
    [
      maintainers: ["thelastinuit"],
      licenses: ["MIT"],
      links: %{
        "Github" => "https://github.com/thelastinuit/sann"
      }
    ]
  end
end
