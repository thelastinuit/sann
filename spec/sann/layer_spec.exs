defmodule SANN.LayerSpec do
  use ESpec, async: true

  context "#start_link" do
    let :layer_result, do: SANN.Layer.start_link()

    it "Test Layer initialization" do
      expect(layer_result()) |> to(be_ok_result())
    end
  end

  context "#add" do
    let_ok :layer_process_id, do: SANN.Layer.start_link(%{neurons: []})
    let_ok :neuron_process_id, do: SANN.Neuron.start_link()

    it "add neuron to layer" do
      {:ok, process_id} = SANN.Layer.add(layer_process_id(), [neuron_process_id()])
      layer = SANN.Layer.find(process_id)
      expect(layer.neurons) |> to(eq [neuron_process_id()])
    end
  end

  context "#clear" do
    let_ok :layer_process_id, do: SANN.Layer.start_link(%{neurons: []})
    let_ok :neuron_process_id, do: SANN.Neuron.start_link()

    it "clears layer" do
      SANN.Layer.add(layer_process_id(), [neuron_process_id()])
      SANN.Layer.clear(layer_process_id())
      layer = SANN.Layer.find(layer_process_id())
      expect(layer.neurons) |> to(eq [])
    end
  end

  context "#set" do
    let_ok :layer_process_id, do: SANN.Layer.start_link(%{neurons: []})
    let_ok :neuron_one_process_id, do: SANN.Neuron.start_link()
    let_ok :neuron_two_process_id, do: SANN.Neuron.start_link()

    it "clears layer" do
      SANN.Layer.add(layer_process_id(), [neuron_one_process_id(), neuron_two_process_id()])
      SANN.Layer.set(layer_process_id(), [neuron_two_process_id()])
      layer = SANN.Layer.find(layer_process_id())
      expect(layer.neurons) |> to(eq [neuron_two_process_id()])
    end
  end

  context "#find" do
    let_ok :layer_process_id, do: SANN.Layer.start_link(%{neurons: []})
    let :layer, do: SANN.Layer.find(layer_process_id())

    it "finds layer" do
      expect(layer().neurons) |> to(eq [])
    end
  end

  context "#update" do
    let_ok :layer_process_id, do: SANN.Layer.start_link(%{neurons: []})
    let_ok :neuron_process_id, do: SANN.Neuron.start_link()
    let :layer_update_result, do: SANN.Layer.update(layer_process_id(), %{neurons: [neuron_process_id()]})

    it "updates layer" do
      expect(layer_update_result()) |> to(be_ok_result())
      {:ok, process_id} = layer_update_result()
      layer = SANN.Layer.find(process_id)
      expect(layer.neurons) |> to(eq [neuron_process_id()])
    end
  end

  context "#train" do
    let_ok :layer_process_id, do: SANN.Layer.start_link(%{neurons: []})

    it "trains neurons in layer" do
      layer = SANN.Layer.find(layer_process_id())
      expect(SANN.Layer.train(layer)) |> to(be_ok_result())
    end
  end

  context "#entangle" do
    let_ok :layer_one_process_id, do: SANN.Layer.start_link(%{neurons: []})
    let_ok :neuron_one_process_id, do: SANN.Neuron.start_link()
    let! :layer_one_update_result, do: SANN.Layer.update(layer_one_process_id(), %{neurons: [neuron_one_process_id()]})

    let_ok :layer_two_process_id, do: SANN.Layer.start_link(%{neurons: []})
    let_ok :neuron_two_process_id, do: SANN.Neuron.start_link()
    let! :layer_two_update_result, do: SANN.Layer.update(layer_two_process_id(), %{neurons: [neuron_two_process_id()]})

    it "entangle neurons among layers" do
      expect(SANN.Layer.entangle(layer_one_process_id(), layer_two_process_id())) |> to(be_ok_result())
    end
  end

  context "#activate" do
    let_ok :layer_process_id, do: SANN.Layer.start_link(%{neurons: []})
    let_ok :neuron_process_id, do: SANN.Neuron.start_link()
    let! :layer_update_result, do: SANN.Layer.update(layer_process_id(), %{neurons: [neuron_process_id()]})

    it "activate neurons in layer" do
      expect(SANN.Layer.activate(layer_process_id())) |> to(be_ok_result())
    end
  end
end
