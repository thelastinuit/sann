defmodule SANN.NeuronSpec do
  use ESpec, async: true

  context "#learning_rate" do
    it "returns learning rate" do
      expect(SANN.Neuron.learning_rate()) |> to(eq 0.382)
    end
  end

  context "#start_link" do
    let :neuron_result, do: SANN.Neuron.start_link()

    it "Test Neuron initialization" do
      expect(neuron_result()) |> to(be_ok_result())
    end
  end

  context "#find" do
    let_ok :neuron_process_id, do: SANN.Neuron.start_link(%{input: 0, output: 0, in: [], out: [], bias?: false, delta: 0})
    let :neuron, do: SANN.Neuron.find(neuron_process_id())

    it "finds neuron" do
      expect(neuron().delta) |> to(eq 0)
    end
  end

  context "#update" do
    let_ok :neuron_process_id, do: SANN.Neuron.start_link(%{input: 0, output: 0, in: [], out: [], bias?: false, delta: 0})
    let :neuron_update_result, do: SANN.Neuron.update(neuron_process_id(), %{delta: 0.5})

    it "updates neuron" do
      expect(neuron_update_result()) |> to(be_ok_result())
      {:ok, process_id} = neuron_update_result()
      neuron = SANN.Neuron.find(process_id)
      expect(neuron.delta) |> to(eq 0.5)
    end
  end

  context "#entangle" do
    let_ok :neuron_one_process_id, do: SANN.Neuron.start_link(%{input: 0, output: 0, in: [], out: [], bias?: false, delta: 0.1})
    let_ok :neuron_two_process_id, do: SANN.Neuron.start_link(%{input: 0, output: 0, in: [], out: [], bias?: false, delta: 0.2})

    it "entangles neurons" do
      {:ok, connection_process_id} = SANN.Neuron.entangle(neuron_one_process_id(), neuron_two_process_id())
      connection = SANN.Connection.find(connection_process_id)
      expect(connection.source_process_id) |> to(eq neuron_one_process_id())
      expect(connection.target_process_id) |> to(eq neuron_two_process_id())
    end
  end

  context "#activate" do
    let_ok :unbiased_neuron_process_id, do: SANN.Neuron.start_link(%{input: 0, output: 0, in: [], out: [], bias?: false, delta: 0})
    let_ok :biased_neuron_process_id, do: SANN.Neuron.start_link(%{input: 0, output: 0, in: [], out: [], bias?: true, delta: 0})

    it "activates unbiased neuron" do
      {:ok, process_id} = SANN.Neuron.activate(unbiased_neuron_process_id(), 1)
      neuron = SANN.Neuron.find(process_id)
      expect(neuron.output) |> to(eq 0.7310585786300049)
    end

    it "activates biased neuron" do
      {:ok, process_id} = SANN.Neuron.activate(biased_neuron_process_id(), 1)
      neuron = SANN.Neuron.find(process_id)
      expect(neuron.output) |> to(eq 1)
    end
  end

  context "#train" do
    let_ok :unbiased_neuron_process_id, do: SANN.Neuron.start_link(%{input: 0, output: 0, in: [1], out: [], bias?: false, delta: 0})
    let_ok :biased_neuron_process_id, do: SANN.Neuron.start_link(%{input: 0, output: 0, in: [], out: [], bias?: true, delta: 0})

    it "trains unbiased neuron" do
      {:ok, _connection_process_id} = SANN.Neuron.entangle(unbiased_neuron_process_id(), biased_neuron_process_id())
      {:ok, process_id} = SANN.Neuron.train(unbiased_neuron_process_id(), 1)
      neuron = SANN.Neuron.find(process_id)
      expect(neuron.delta) |> to(eq 0.0)
    end

    it "trains biased neuron" do
      {:ok, process_id} = SANN.Neuron.train(biased_neuron_process_id(), 1)
      neuron = SANN.Neuron.find(process_id)
      expect(neuron.delta) |> to(eq 0)
    end
  end
end
