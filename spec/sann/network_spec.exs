defmodule SANN.NetworkSpec do
  use ESpec, async: true

  context "#start_link" do
    let :network_result, do: SANN.Network.start_link()

    it "Test Network initialization" do
      expect(network_result()) |> to(be_ok_result())
    end
  end

  context "#find" do
    let_ok :network_process_id, do: SANN.Network.start_link()
    let :network, do: SANN.Network.find(network_process_id())

    it "finds layer" do
      expect(network().error) |> to(eq 0)
    end
  end

  context "#update" do
    let_ok :network_process_id, do: SANN.Network.start_link()
    let :network_update_result, do: SANN.Network.update(network_process_id(), %{error: 1})

    it "updates layer" do
      expect(network_update_result()) |> to(be_ok_result())
      {:ok, process_id} = network_update_result()
      network = SANN.Network.find(process_id)
      expect(network.error) |> to(eq 1)
    end
  end

  context "#train" do
    let_ok :input_layer_process_id, do: SANN.Layer.start_link(%{neurons: []})
    let_ok :input_neuron_process_id, do: SANN.Neuron.start_link()
    let! :input_layer_update_result, do: SANN.Layer.update(input_layer_process_id(), %{neurons: [input_neuron_process_id()]})

    let_ok :output_layer_process_id, do: SANN.Layer.start_link(%{neurons: []})
    let_ok :output_neuron_process_id, do: SANN.Neuron.start_link()
    let! :output_layer_update_result, do: SANN.Layer.update(output_layer_process_id(), %{neurons: [output_neuron_process_id()]})

    let_ok :hidden_layer_process_id, do: SANN.Layer.start_link(%{neurons: []})
    let_ok :hidden_neuron_process_id, do: SANN.Neuron.start_link()
    let! :hidden_layer_update_result, do: SANN.Layer.update(hidden_layer_process_id(), %{neurons: [hidden_neuron_process_id()]})

    let_ok :network_process_id, do: SANN.Network.start_link()
    let! :network_update_process_id, do: SANN.Network.update(network_process_id(), %{input_layer: input_layer_process_id(), output_layer: output_layer_process_id(), hidden_layers: [hidden_layer_process_id()]})

    it "trains network" do
      network = SANN.Network.find(network_process_id())
      expect(SANN.Network.train(network, [1])) |> to(be_ok_result())
    end
  end

  context "#activate" do
let_ok :input_layer_process_id, do: SANN.Layer.start_link(%{neurons: []})
    let_ok :input_neuron_process_id, do: SANN.Neuron.start_link()
    let! :input_layer_update_result, do: SANN.Layer.update(input_layer_process_id(), %{neurons: [input_neuron_process_id()]})

    let_ok :output_layer_process_id, do: SANN.Layer.start_link(%{neurons: []})
    let_ok :output_neuron_process_id, do: SANN.Neuron.start_link()
    let! :output_layer_update_result, do: SANN.Layer.update(output_layer_process_id(), %{neurons: [output_neuron_process_id()]})

    let_ok :hidden_layer_process_id, do: SANN.Layer.start_link(%{neurons: []})
    let_ok :hidden_neuron_process_id, do: SANN.Neuron.start_link()
    let! :hidden_layer_update_result, do: SANN.Layer.update(hidden_layer_process_id(), %{neurons: [hidden_neuron_process_id()]})

    let_ok :network_process_id, do: SANN.Network.start_link()
    let! :network_update_process_id, do: SANN.Network.update(network_process_id(), %{input_layer: input_layer_process_id(), output_layer: output_layer_process_id(), hidden_layers: [hidden_layer_process_id()]})

    it "activates network" do
      network = SANN.Network.find(network_process_id())
      expect(SANN.Network.activate(network, [1])) |> to(be_ok_result())
    end
  end
end
