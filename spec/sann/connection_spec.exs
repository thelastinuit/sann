defmodule SANN.ConnectionSpec do
  use ESpec, async: true

  context "#start_link" do
    let :connection_result, do: SANN.Connection.start_link()

    it "Test Connection initialization" do
      expect(connection_result()) |> to(be_ok_result())
    end
  end

  context "#find" do
    let_ok :connection_process_id, do: SANN.Connection.start_link(%{source_process_id: nil, target_process_id: nil, weight: 0.73})
    let :connection, do: SANN.Connection.find(connection_process_id())

    it "finds connection" do
      expect(connection().weight) |> to(eq 0.73)
    end
  end

  context "#update" do
    let_ok :connection_process_id, do: SANN.Connection.start_link(%{source_process_id: nil, target_process_id: nil, weight: 0.73})
    let :connection_update_result, do: SANN.Connection.update(connection_process_id(), %{weight: 0.5})

    it "updates connection" do
      expect(connection_update_result()) |> to(be_ok_result())
      {:ok, process_id} = connection_update_result()
      connection = SANN.Connection.find(process_id)
      expect(connection.weight) |> to(eq 0.5)
    end
  end

  context "#entangle" do
    let_ok :neuron_one_process_id, do: SANN.Neuron.start_link()
    let_ok :neuron_two_process_id, do: SANN.Neuron.start_link()

    it "entangles neurons" do
      {:ok, connection_process_id} = SANN.Connection.entangle(neuron_one_process_id(), neuron_two_process_id())
      connection = SANN.Connection.find(connection_process_id)
      expect(connection.source_process_id) |> to(eq neuron_one_process_id())
      expect(connection.target_process_id) |> to(eq neuron_two_process_id())
    end
  end
end
