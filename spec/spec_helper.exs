ESpec.configure fn(config) ->
  config.before fn(tags) ->
    {:shared, hello: :world, tags: tags}
  end

  config.finally fn(_shared) ->
    :ok
  end

  config.formatters [
    {ESpec.Formatters.Doc, %{diff_enabled?: true}},
    {ESpec.Formatters.Json, %{out_path: "results.json"}},
    {ESpec.Formatters.Html, %{out_path: "results.html"}}
  ]
end
